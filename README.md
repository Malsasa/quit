# List of Resources in Quitting Facebook

This is my list of resources about quitting Facebook including links from the famous Degooglify Internet and #DeleteFacebook Movement. This list is for everyone who wants to quit, escape, avoid, or reduce Facebook in their life and also 

# The Five Resources
These five links are taken from my post on Write.as **Why Quit Facebook** which also inspires this list:
1) [**Why you should delete facebook**](https://www.fsf.org/facebook) (Free Software Foundation)
2) [**What is at stake if you keep facebook?**](https://degooglisons-internet.org/en/#enjeux) (Degooglify Movement)
3) [**Reminders why you left facebook**](https://deletefacebook.com/) (#DeleteFacebook Movement)
4) [**It's time to leave Gafam**](https://www.laquadrature.net/en/2018/04/18/class_action_gafam) (La Quadrature Du Net)
5) [**Global surveillance 2013**](https://en.wikipedia.org/wiki/Global_surveillance_disclosures_(2013%E2%80%93present)) (Wikipedia)

# Account Deletion Procedures
...work in progress...